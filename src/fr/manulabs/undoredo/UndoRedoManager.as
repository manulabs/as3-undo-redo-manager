//------------------------------------------------------------------------------
//
//   ManuLabs 2014 
// 
//   Author: Emmanuel Dupont 
//   Email: emmanuel@manulabs.fr 
//
//------------------------------------------------------------------------------

package fr.manulabs.undoredo
{
	import flash.events.Event;
	import flash.events.EventDispatcher;


	public class UndoRedoManager extends EventDispatcher
	{

		public function UndoRedoManager()
		{
		}

		public var MAXLENGTH:Number        = 30;

		/* *********************
		   UNDO - REDO LOGIC
		 ********************* */

		private var _history:Array         = [];


		[Bindable("indexChanged")]
		public function get canRedo():Boolean
		{
			return index < _history.length - 1;
		}


		[Bindable("indexChanged")]
		public function get canUndo():Boolean
		{
			return index >= 0;
		}

		public var closureFactory:Function = function(transform:Function, ... args):Function
		{
			var closure:Function = function():void
			{
				transform.apply(null, args);
			}
			return closure;
		}

		private var _index:int             = -1;


		protected function get index():Number
		{
			return _index;
		}


		/**
		 * Updates the current stack index.
		 *
		 * <p>
		 * Updates the current stack index and dispatches the indexChanged
		 * event.
		 * </p>
		 */
		protected function set index(ix:Number):void
		{
			_index = ix;
			dispatchEvent(new Event("indexChanged"));
		}


		public function get stackIndex():Number
		{
			return _index;
		}


		public function updateHistory(undoTransform:Function, redoTransform:Function):void
		{
			if (_history.length == MAXLENGTH)
			{
				_history.shift();
			}
			else
			{
				index++;
			}

			_history.splice(index);

			_history.push({forwardTransfrom: redoTransform, reverseTransform: undoTransform});
		}


		public function clearLast():void
		{
			if (_history.length > 0)
			{
				if (index == _history.length - 1)
				{
					index--;
				}
				_history.pop();
			}
		}


		public function undo():void
		{
			if (canUndo)
			{
				var closure:Function = _history[index].reverseTransform;
				index--;
				closure();
			}
		}


		public function redo():void
		{
			if (canRedo)
			{
				index++;
				var closure:Function = _history[index].forwardTransfrom;
				closure();
			}
		}


		public function clear():void
		{
			_history = [];
			index = -1;
		}
	}
}

